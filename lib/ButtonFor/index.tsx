export type ButtonForProps = React.ComponentProps<'button'> & {
	htmlFor: string | HTMLDialogElement | HTMLInputElement | null;
};

const ButtonFor = (props: ButtonForProps) => (
	<button
		{...props}
		onClick={(event) => {
			let popUp = props.htmlFor;

			if (typeof props.htmlFor === 'string') {
				popUp = document.getElementById(props.htmlFor) as HTMLDialogElement;
			}

			if (popUp instanceof HTMLDialogElement) {
				popUp.open = !popUp.open;
			}

			if (popUp instanceof HTMLInputElement) {
				popUp.checked = !popUp.checked;
			}

			props.onClick && props.onClick(event);
		}}
	>
		{props.children}
	</button>
);

export default ButtonFor;
