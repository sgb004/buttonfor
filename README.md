# ButtonFor

[![Version](https://img.shields.io/badge/version-1.0.0-blue.svg)](https://www.npmjs.com/package/@wudev/button-for)
[![Author](https://img.shields.io/badge/author-sgb004-green.svg)](https://sgb004.com)

This component adds a `for` attribute to a `<button />` so that it works similarly to how the `<label />` tag does, with the particularity that it works to open or close `<dialog />` tags.

## Installation

To install **ButtonFor** you can use npm:

```
npm install @wudev/button-for
```

<!-- examples -->

## Usage

Import the component and add the `htmlFor` attribute to tell it witch element it should interact with.

It is possible to pass it an id like `<label />` tag or pass it a reference to a `<dialog />`or `<input type="checkbox|radio" />`.

<!-- prettier-ignore-start -->
```jsx
import ButtonFor from '@wudev/button-for';

<ButtonFor htmlFor="id-of-dialog-or-input-checkbox-radio">Click me!</ButtonFor>
<ButtonFor htmlFor={refOfDialogOrInputCheckboxRadio}>Click me!</ButtonFor>
```
<!-- prettier-ignore-end -->

## Author

[sgb004](https://sgb004.com)

## License

[MIT](LICENSE)
