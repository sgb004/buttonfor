import React from 'react';
import ReactDOM from 'react-dom/client';
import Readme from './Readme.tsx';

ReactDOM.createRoot(document.getElementById('root')!).render(
	<React.StrictMode>
		<div className="content">
			<Readme />
		</div>
	</React.StrictMode>
);
