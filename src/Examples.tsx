import { useEffect, useRef, useState } from 'react';
import ButtonFor from '../lib/ButtonFor';
import './example.css';

const Examples = () => {
	const checkboxRef = useRef<HTMLInputElement>(null);
	const [checkbox, setCheckbox] = useState<HTMLInputElement | null>(null);
	const dialogRef = useRef<HTMLDialogElement>(null);
	const [dialog, setDialog] = useState<HTMLDialogElement | null>(null);

	useEffect(() => {
		setCheckbox(checkboxRef.current);
		setDialog(dialogRef.current);
	}, []);

	return (
		<div id="examples">
			<h2>Examples</h2>

			<div className="example example-1">
				<label>
					<input id="checkbox" type="checkbox" />
					<span>Checkbox with id</span>
				</label>
				<ButtonFor htmlFor="checkbox">Click me to enable / disable the checkbox</ButtonFor>
			</div>
			<div className="example example-2">
				<label>
					<input type="checkbox" ref={checkboxRef} />
					<span>Checkbox by ref</span>
				</label>
				<ButtonFor htmlFor={checkbox}>Click me to enable / disable the checkbox</ButtonFor>
			</div>
			<div className="example example-3">
				<dialog id="dialog">
					<p>This a dialog opened with a button with the attribute for using its id.</p>
					<ButtonFor htmlFor="dialog">Close</ButtonFor>
				</dialog>
				<ButtonFor htmlFor="dialog">Open a dialog by id</ButtonFor>
			</div>
			<div className="example example-4">
				<dialog ref={dialogRef}>
					<p>This a dialog opened using the attribute ref.</p>
					<ButtonFor htmlFor={dialog}>Close</ButtonFor>
				</dialog>
				<ButtonFor htmlFor={dialog}>Open a dialog by ref</ButtonFor>
			</div>
		</div>
	);
};

export default Examples;
