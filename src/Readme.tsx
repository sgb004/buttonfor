import Examples from './Examples';

const Readme = () => (
	<div id="readme">
	<h1>ButtonFor</h1>
<p><a href="https://www.npmjs.com/package/@wudev/button-for"><img src="https://img.shields.io/badge/version-1.0.0-blue.svg" alt="Version" /></a>
<a href="https://sgb004.com"><img src="https://img.shields.io/badge/author-sgb004-green.svg" alt="Author" /></a></p>
<p>This component adds a <code>for</code> attribute to a <code>&lt;button /&gt;</code> so that it works similarly to how the <code>&lt;label /&gt;</code> tag does, with the particularity that it works to open or close <code>&lt;dialog /&gt;</code> tags.</p>
<h2>Installation</h2>
<p>To install <strong>ButtonFor</strong> you can use npm:</p>
<pre><code>npm install @wudev/button-for<br/></code></pre>
<Examples />
<h2>Usage</h2>
<p>Import the component and add the <code>htmlFor</code> attribute to tell it witch element it should interact with.</p>
<p>It is possible to pass it an id like <code>&lt;label /&gt;</code> tag or pass it a reference to a <code>&lt;dialog /&gt;</code>or <code>&lt;input type="checkbox|radio" /&gt;</code>.</p>
{/* prettier-ignore-start */}
<pre><code>import ButtonFor from '@wudev/button-for';<br/><br/>&lt;ButtonFor htmlFor="id-of-dialog-or-input-checkbox-radio"&gt;Click me!&lt;/ButtonFor&gt;<br/>&lt;ButtonFor htmlFor=&#123;refOfDialogOrInputCheckboxRadio&#125;&gt;Click me!&lt;/ButtonFor&gt;<br/></code></pre>
{/* prettier-ignore-end */}
<h2>Author</h2>
<p><a href="https://sgb004.com">sgb004</a></p>
<h2>License</h2>
<p><a href="LICENSE">MIT</a></p>
	</div>
);

export default Readme;
	